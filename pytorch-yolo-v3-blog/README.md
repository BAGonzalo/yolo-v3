# Basics

Turorial on building []+ weight loading a pre-trained YOLOv3-object detector from scratch (no training code provided). Blog post:

https://blog.paperspace.com/how-to-implement-a-yolo-object-detector-in-pytorch/

## Additional resources

YOLOv3 full code implementation: https://github.com/ayooshkathuria/pytorch-yolo-v3

Other good post: https://tryolabs.com/blog/2018/01/18/faster-r-cnn-down-the-rabbit-hole-of-modern-object-detection/
... with its own implementation [in Tensorflow]: https://github.com/tryolabs/luminoth/tree/master/luminoth/models/fasterrcnn

Other implementations:
- https://github.com/chenyuntc/simple-faster-rcnn-pytorch

## Setting up

To run the code and notebooks, use the image dockerfile provided [CPU-only support], as follows:

[1] Install [Docker Engine](https://www.docker.com/community-edition#/download); and clone the repository.

[2] Build the image, and run the environment by:

```
cd test-switchnorm
docker build -t cpu -f dockerfile_cpu .
docker run -it -v {global-path-to-repo}/test-switchnorm:/test-switchnorm -p 9000:9000 --user root --shm-size 16G --name test cpu
cd test-switchnorm
jupyter notebook --ip 0.0.0.0 --no-browser --port 9000 --allow-root 

```

To enter the running container from another console window, run:

```
docker exec -it test bin/bash
```

[3] Access the Jupyter notebook tree at http://localhost:9000/, and enter the token provided by the console -e.g.:

```
[I 13:30:53.425 NotebookApp] The Jupyter Notebook is running at:
[I 13:30:53.426 NotebookApp] http://809b15d36bae:9000/?token=5b849c9e10fc2671579c113e270f3953ad33113120ce28d6
```

The repo will be visualised as a files tree in your browser.

[4] Open and run the notebook/s.

## Authors

* **BAGonzalo** - https://gitlab.com/BAGonzalo (bagonzalo@gmail.com)